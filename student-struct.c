#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct Student{
  char *firstName;
  char *subject;
  int marks;
};

int readData(struct Student *student){ 
  printf("Enter first name : ");
  scanf("%[^\n]%*c",student->firstName);
  printf("Enter subject : ");
  scanf("%[^\n]%*c",student->subject);
  printf("Enter marks : ");
  scanf("%d",&student->marks);
}  

int printData(struct Student *student,int nameLength,int subjectLength,int index){
  printf("Student %d : \n",index+1);
  printf("Student's name : %.*s \nSubject : %.*s \nMarks : %d \n\n",nameLength,student->firstName,subjectLength,student->subject,student->marks);
}

int main(){
  int count = 0,numOfStudents = 0;
  int nameLen = 0,subLen = 0;
  printf("Enter number of students : ");
  scanf("%d",&numOfStudents);
  int nameLengths[numOfStudents];
  int subjectLengths[numOfStudents];
  struct Student students[numOfStudents];
  while(count<numOfStudents){
    printf("For student %d : \n",count+1);
    printf("Enter name length : ");
    scanf("%d",&nameLen);
    nameLengths[count] = nameLen;
    printf("Enter subject length : ");
    scanf("%d",&subLen);
    getchar();
    subjectLengths[count] = subLen;
    students[count].firstName = (char*)malloc(nameLen*sizeof(char));
    students[count].subject = (char*)malloc(subLen*sizeof(char));   
    readData(&students[count]);
    count++;
  }
  printf("\n");
  count = 0;
  while(count<numOfStudents){
    printData(&students[count],nameLengths[count],subjectLengths[count],count);
    count++;
  }  
  
}  
